INF2100 - Prosjektoppgave i programmering


Oblig 1 - Scanner
* Ferdig og levert

Oblig 2 - Parser
1. Implementere en parser, med en klasse for hver ikke-terminal
    Statement eksempel:
    min 10: https://screencast.uninett.no/relay/ansatt/daguio.no/2015/23.09/2524267/INF2100_uke_39_2015_del_1_-_20150923_130059_39.html


2. Skrive en egnet metode parse i hver av disse klassene slik at grammatikkfeil blir oppdaget og syntakstreet bygget sammen
    parse():
    det er lett å kalle readNextToken() for ofte eller for sjelden.
	- når man kaller parse, skal første symbol være lest inn.
	- når man returnerer fra en parse, ksla  første symbol etter konstruksjonen være lest.
	vært spesielt oppmerksom der du har forgreninger og løkker i jerbanediagrammet

3. sørge for loggin a la -lopgP og -logY 
    Testutskrifter:
    -logP avslører om man gjort riktigt valg i jernbanediagrammet, la hver parse gi lyd fra seg.


   -logY sjekker om analysetreet ble riktig ved å skrive det ut etterpå
   "pretty print"

    -testparser slår på begge disse to og stopper etter parseringen


    Alle klasser som vi kan opprette objekter av skal ha en metode:


    @Override void prettyPrint(){

   }
   min 31 del 1 av forelesninga

4. M� fikse problem i Scanner med slutten av en fil. Blir problem med endToken, dotToken og eofToken for parseren.



Oblig 3 - Checker
*

Oblig 4 - Codegenerator
*