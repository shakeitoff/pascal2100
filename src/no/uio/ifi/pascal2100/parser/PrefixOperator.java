package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * PrefixOperator class, extends Operator
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class PrefixOperator extends Operator {
	TokenKind tk;

	public PrefixOperator(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<prefix opr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(tk.toString() + " ");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return po PrefixOperator object Creates new prefix operator object
	 *         checks what kind of prefix operator the current token is sets
	 *         local TokenKind tk to that value. returns the prefix operator
	 *         object.
	 */
	static PrefixOperator parse(Scanner s) {
		enterParser("prefix opr");
		PrefixOperator po = new PrefixOperator(s.curLineNum());

		if (s.curToken.kind == addToken) {
			po.tk = addToken;
			s.skip(addToken);
		} else if (s.curToken.kind == subtractToken) {
			po.tk = subtractToken;
			s.skip(subtractToken);
		} else {
			System.out.println("unknown Prefix Operator " + s.curToken.identify());
		}
		leaveParser("prefix opr");
		return po;
	}

	@Override
	void check(Block curScope, Library lib) {
		

	}

	@Override
	void genCode(CodeFile f) {
		switch(tk) {
		case subtractToken:
		    f.genInstr("", "negl", "%eax", "Compute subtract prefix");
		    break;
		case addToken:
		    //f.genInstr("", "movl", "(%eax),%eax", "Compute add prefix");
		    break;
		}
		
	}

}
