package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Negation class, extends Factor
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class Negation extends Factor {

   Factor factor;

   Negation(int lineNum) {

      super(lineNum);
   }

   @Override
      public String identify() {
	 return "<negation> on line " + lineNum;
      }

   @Override
      void prettyPrint() {
	 Main.log.prettyPrint("not ");
	 factor.prettyPrint();
      }

   /**
    * @param s
    *            Scanner object
    * @return neg Negation object Skips not token, creates new negation object,
    *         sets the local factor refrence and returns the negation object.
    */
   static Negation parse(Scanner s) {
      enterParser("negation");

      s.skip(notToken);
      Negation neg = new Negation(s.curLineNum());
      neg.factor = Factor.parse(s);

      leaveParser("negation");
      return neg;
   }

   @Override
      void check(Block curScope, Library lib) {
	 factor.check(curScope, lib);
      }

   @Override
      void genCode(CodeFile f) {
	 factor.genCode(f);
	 f.genInstr("", "xorl", "$0x1,%eax", "not "); 
      }
}
