package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ParamDeclList class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ParamDeclList extends PascalSyntax {

	List<ParamDecl> pdList;

	public ParamDeclList(int n) {
		super(n);
	}

	@Override
	public String identify() {
		return "<param decl list> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("(");
		int i = 1;
		for (ParamDecl par : pdList) {
			par.prettyPrint();
			if (i < pdList.size()) {
				Main.log.prettyPrint("; ");
			}
			i++;
		}

		Main.log.prettyPrint(")");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return pdl ParamDeclList object Creates new ParamDeclList object, parses
	 *         the parameter declarations and adds them to the local arraylist
	 *         pdList. Returns the ParamDecl list object pdl.
	 */
	static ParamDeclList parse(Scanner s) {
		enterParser("param decl list");

		s.skip(leftParToken);
		ParamDeclList pdl = new ParamDeclList(s.curLineNum());
		pdl.pdList = new ArrayList<ParamDecl>();
		pdl.pdList.add(ParamDecl.parse(s));

		while (s.curToken.kind != rightParToken) {
			s.skip(semicolonToken);
			pdl.pdList.add(ParamDecl.parse(s));
		}

		s.skip(rightParToken);

		leaveParser("param decl list");
		return pdl;
	}

	@Override
	void check(Block curScope, Library lib) {
		int offset = 8;
		for (ParamDecl p : pdList) {
			if (p != null) {
				p.declOffset = offset;
				p.check(curScope, lib);
				offset += 4;
			}
		}
	}

	@Override
	void genCode(CodeFile f) {
		Collections.reverse(pdList);
		for (ParamDecl pd : pdList) {
			pd.genCode(f);
		}
	}

}
