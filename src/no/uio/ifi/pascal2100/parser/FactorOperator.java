package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * FactorOperator class, extends Operator
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
class FactorOperator extends Operator {
	TokenKind tk;

	FactorOperator(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<factor opr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(" " + tk.toString() + " ");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return factOpr FactorOperator object Creates empty factor operator
	 *         object, finds out what kind of operator the current token is,
	 *         sets the local tokenKind object tk to reference this operator
	 *         kind. Returns the factor operator object.
	 */
	static FactorOperator parse(Scanner s) {
		enterParser("factor opr");

		FactorOperator factOpr = null;

		switch (s.curToken.kind) {
		case multiplyToken:
			factOpr = new FactorOperator(s.curLineNum());
			factOpr.tk = multiplyToken;
			s.skip(multiplyToken);
			break;
		case divToken:
			factOpr = new FactorOperator(s.curLineNum());
			factOpr.tk = divToken;
			s.skip(divToken);
			break;
		case modToken:
			factOpr = new FactorOperator(s.curLineNum());
			factOpr.tk = modToken;
			s.skip(modToken);
			break;
		case andToken:
			factOpr = new FactorOperator(s.curLineNum());
			factOpr.tk = andToken;
			s.skip(andToken);
			break;
		default:
			System.out.println("Error: unknown factor operator");
			break;
		}

		leaveParser("factor opr");
		return factOpr;
	}

	@Override
	void check(Block curScope, Library lib) {
		

	}

	@Override
	void genCode(CodeFile f) {
	    f.genInstr("", "movl", "%eax,%ecx", "");
	    f.genInstr("", "popl", "%eax", "");
	    
	    switch(tk) {
	    case multiplyToken:
	        f.genInstr("", "imull", "%ecx,%eax", "*");
	        break;
	    case divToken:
	        f.genInstr("", "cdq", "", "");
	        f.genInstr("", "idivl", "%ecx", "/");
	        break;
	    case modToken:
	        f.genInstr("", "cdq", "", "");
            f.genInstr("", "idivl", "%ecx", "");
            f.genInstr("", "movl", "%edx,%eax", "mod");
	        break;
	    case andToken:
	        f.genInstr("", "andl", "%ecx,%eax", "and");
	        break;
	    }
	}

}
