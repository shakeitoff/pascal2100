package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Abstract Factor class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
abstract class Factor extends PascalSyntax {
	Factor fac;

	Factor(int lineNum) {

		super(lineNum);
	}

	@Override
	public String identify() {
		return "<factor> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return Factor object finds what kind of factor the token is, parses and
	 *         sets local variable fac. Then returns the factor object.
	 */
	static Factor parse(Scanner s) {
		enterParser("factor");

		Factor factor = null;

		switch (s.curToken.kind) {
		case stringValToken:
			factor = Constant.parse(s);
			break;
		case intValToken:
			factor = Constant.parse(s);
			break;
		case nameToken:
			switch (s.nextToken.kind) {
			case leftParToken:
				factor = FuncCall.parse(s);
				break;
			case leftBracketToken:
				factor = Variable.parse(s);
				break;
			default:
				factor = Variable.parse(s);
				break;
			}
			break;
		case leftParToken:
			factor = InnerExpr.parse(s);
			break;
		case notToken:
			factor = Negation.parse(s);
			break;
		case subtractToken:
			Main.error("Error in line " + s.curLineNum() + ": " + "Expected a value, but found a " + s.curToken.kind
					+ "!");
			break;
		case addToken:
			Main.error("Error in line " + s.curLineNum() + ": " + "Expected a value, but found a " + s.curToken.kind
					+ "!");
			break;
		default:
			factor = Variable.parse(s);
			break;
		}

		leaveParser("factor");
		return factor;
	}
}
