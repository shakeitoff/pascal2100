package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * TermOperator class, extends Operator
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class TermOperator extends Operator {
	TokenKind tk;

	public TermOperator(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<term opr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(" " + tk.toString() + " ");

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return to TermOperator object Creates new TermOperator object checks
	 *         what kind of term operator the current token is, sets local
	 *         TokenKind tk variable returns the TermOperator object
	 */
	static TermOperator parse(Scanner s) {
		enterParser("term opr");

		TermOperator to = new TermOperator(s.curLineNum());

		if (s.curToken.kind == addToken) {
			to.tk = addToken;
			s.skip(addToken);
		} else if (s.curToken.kind == subtractToken) {
			to.tk = subtractToken;
			s.skip(subtractToken);
		} else if (s.curToken.kind == orToken) {
			to.tk = orToken;
			s.skip(orToken);
		} else {
			System.out.println("Unknown tokenkind in TermOperator: " + s.curToken.identify());
		}
		leaveParser("term opr");
		return to;

	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
	    f.genInstr("", "movl", "%eax,%ecx", "");
	    f.genInstr("", "popl", "%eax", "");
		
	    switch(tk) {
	    case addToken:
	        f.genInstr("", "addl", "%ecx,%eax", "+");
	        break;
	    case subtractToken:
	        f.genInstr("", "subl", "%ecx,%eax", "-");
	        break;   
	    case orToken:
	        f.genInstr("", "orl", "%ecx,%eax", "or");
	        break;
	    }
		
	}

}
