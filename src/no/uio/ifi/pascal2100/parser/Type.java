package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Type class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public abstract class Type extends PascalSyntax {

   Type(int lineNum) {
      super(lineNum);
   }

   @Override
      public String identify() {
	 return "<type> on line " + lineNum;
      }

   /**
    * @param s
    *            Scanner object
    * @return t Type object Creates empty Type object, checks what kind of
    *         token curtoken is, sets the Type object to be the right kind of
    *         type and returns it.
    */
   static Type parse(Scanner s) {
      Type t = null;

      switch (s.curToken.kind) {
	 case intValToken:
	    t = RangeType.parse(s);
	    break;
	 case leftParToken:
	    t = EnumType.parse(s);
	    break;
	 case arrayToken:
	    t = ArrayType.parse(s);
	    break;
	 default:
	    t = TypeName.parse(s);
	    break;
      }

      return t;
   }

}
