package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * EnumLiteral class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class EnumLiteral extends PascalDecl {
    int value = 0;

    public EnumLiteral(String id, int lNum) {
        super(id, lNum);
    }

    @Override
    public String identify() {
        if (lineNum > 0) {
            return "<enum literal> on line " + lineNum;
        } else {
            return "<enum literal>";
        }
    }

    @Override
    void prettyPrint() {
        Main.log.prettyPrint(name + " ");
    }

    /**
     * @param s
     *            Scanner object
     * @return el EnumLiteral object test for name token, creates EnumLiteral
     *         object read next token and return the EnumLiteral created.
     */
    static EnumLiteral parse(Scanner s) {
        enterParser("enum literal");

        s.test(nameToken);
        EnumLiteral el = new EnumLiteral(s.curToken.id, s.curLineNum());
        s.readNextToken();

        leaveParser("enum literal");
        return el;
    }

    @Override
    void check(Block curScope, Library lib) {
        curScope.addDecl(name, this);
    }

    @Override
    void genCode(CodeFile f) {
        if (name.equals("false")) {
            f.genInstr("", "movl", "$0,%eax", "Enum value false");
        } else if (name.equals("true")) {
            f.genInstr("", "movl", "$1,%eax", "Enum value true");
        } else {
            f.genInstr("", "movl", "$" + value + ",%eax", "Enum value " + value);
        }
    }

}
