package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.scanner.*;

/**
 * ConstDecl class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ConstDecl extends PascalDecl {

	Constant con;

	ConstDecl(String id, int lineNum) {
		super(id, lineNum);

	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint(name);
		Main.log.prettyPrint(" = ");
		con.prettyPrint();
		Main.log.prettyPrint("; ");

	}

	@Override
	public String identify() {
		if(lineNum > 0) {
			return "<const decl> on line " + lineNum;
		} else {
			return "<const decl>";
		}
		
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return cd ConstDecl object Tests for a name token, creates a new
	 *         ConstDecl object parses the constant and sets the local Constant
	 *         object to refrence the returned constant. returns the constant
	 *         declarations object.
	 */
	static ConstDecl parse(Scanner s) {
		enterParser("const decl");

		s.test(nameToken);
		ConstDecl cd = new ConstDecl(s.curToken.id, s.curLineNum());
		s.readNextToken();

		s.skip(equalToken);

		cd.con = Constant.parse(s);
		s.skip(semicolonToken);

		leaveParser("const decl");
		return cd;
	}

	@Override
	void check(Block curScope, Library lib) {
		con.check(curScope, lib);
	}

	@Override
	void genCode(CodeFile f) {
		if(con != null) {
			con.genCode(f);
			
		}
		
	}
}
