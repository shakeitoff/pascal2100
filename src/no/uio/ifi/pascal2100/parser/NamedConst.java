package no.uio.ifi.pascal2100.parser;

import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;

/**
 * NamedConst class, extends Constant
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class NamedConst extends Constant {
	String name;

	NamedConst(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<named const> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(name);
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return nc NamedConstant object Creates a new NamedConst, sets it local
	 *         name variable to the current tokens id value. Reads next token
	 *         and returns the NamedConst object.
	 */
	static NamedConst parse(Scanner s) {
		enterParser("named const");

		NamedConst nc = new NamedConst(s.curLineNum());
		nc.name = s.curToken.id;
		s.readNextToken();

		leaveParser("named const");
		return nc;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
	}
}
