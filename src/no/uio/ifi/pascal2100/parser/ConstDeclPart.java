package no.uio.ifi.pascal2100.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * ConstDeclPart extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class ConstDeclPart extends PascalSyntax {

	ArrayList<ConstDecl> cdList;

	ConstDeclPart(int lineNum) {

		super(lineNum);
	}

	@Override
	public String identify() {
		return "<const decl part> on line " + lineNum;
	}

	/**
	 * calls prettyprint for for all the elements in cdList
	 */
	@Override
	void prettyPrint() {
		Main.log.prettyPrint("const ");

		Iterator<ConstDecl> it = cdList.iterator();

		while (it.hasNext()) {
			it.next().prettyPrint();
		}
	}

	/**
	 * @param s
	 *            Scanner object
	 */
	static ConstDeclPart parse(Scanner s) {
		enterParser("const decl part");
		ConstDeclPart cdp = new ConstDeclPart(s.curLineNum());

		cdp.cdList = new ArrayList<ConstDecl>();

		s.skip(constToken);

		while (s.nextToken.kind == equalToken) { // adds all the constant
			// declarations to the list.
			ConstDecl cd = ConstDecl.parse(s);
			cdp.cdList.add(cd);
		}

		leaveParser("const decl part");
		return cdp; // returns the ConstDeclPart-object

	}

	public void check(Block curScope, Library lib) {
		for (ConstDecl c : cdList) {
			if (c != null) {
				c.check(curScope, lib);
			}
		}
	}

	@Override
	void genCode(CodeFile f) {
		Collections.reverse(cdList);
		for(ConstDecl cd: cdList) {
			cd.genCode(f);
		}
		
	}
}
