package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * FuncDecl class, extends ProcDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class FuncDecl extends ProcDecl {

	ParamDeclList pdList;
	Type type;
	Block block;

	public FuncDecl(String id, int lNum) {
		super(id, lNum);
	}

	@Override
	public String identify() {
		return "<func decl> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("function ");
		Main.log.prettyPrint(name);

		if (pdList != null) {
			pdList.prettyPrint();
		}

		Main.log.prettyPrint(": ");
		type.prettyPrint();
		Main.log.prettyPrint(";");
		Main.log.prettyPrintLn();
		block.prettyPrint();
		Main.log.prettyPrint(";");
		Main.log.prettyPrintLn();
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return fDecl FuncDecl object skips function token, creates FuncDecl
	 *         object checks for parameter declaration list, if so parses it and
	 *         sets local object, if not parse the type and block, set local
	 *         variables. Return FuncDecl object.
	 */
	static FuncDecl parse(Scanner s) {
		enterParser("func decl");

		s.skip(functionToken);

		s.test(nameToken);
		FuncDecl fDecl = new FuncDecl(s.curToken.id, s.curLineNum());
		s.readNextToken();

		if (s.curToken.kind != colonToken) {
			fDecl.pdList = ParamDeclList.parse(s);
		}

		s.skip(colonToken);

		fDecl.type = Type.parse(s);

		s.skip(semicolonToken);

		fDecl.block = Block.parse(s);

		s.skip(semicolonToken);

		leaveParser("func decl");
		return fDecl;
	}

	public void check(Block curScope, Library lib) {
		if (pdList != null) {
			for (ParamDecl pd : pdList.pdList) {
				block.addDecl(pd.name.toLowerCase(), pd);
			}
		}

		if (pdList != null) {
			pdList.check(curScope, lib);
		}
		type.check(curScope, lib);
		block.check(curScope, lib);
	}

	@Override
	void genCode(CodeFile f) {
		if(pdList != null) {
			for(ParamDecl pd: pdList.pdList) {
				pd.declLevel = this.declLevel;
			}
		}
		
		String label = f.getLocalLabel();
		block.genCodeProcFunc(f);
		if(pdList != null) {
			pdList.genCode(f);	
		}
		f.genInstr("func$" + this.name + "_" + uniqueId, "", "", "");
		f.genInstr("", "enter", "$" + (32 + block.getLocalVars()) + ",$" + this.declLevel, "");
		block.genCode(f);
		f.genInstr("", "movl", "-32(%ebp),%eax", "");
		f.genInstr("", "leave", "", "");
		f.genInstr("", "ret", "", ""); // leave and return up to "fake" main
										// when program is finished generating
										// assembler

	}
}
