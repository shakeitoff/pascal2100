package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * ArrayType class, extends Type
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ArrayType extends Type {
	Type t;
	Type t2;

	ArrayType(int lineNum) {
		super(lineNum);
	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint("array [");
		this.t.prettyPrint();
		Main.log.prettyPrint("] of ");
		this.t2.prettyPrint();
	}

	@Override
	public String identify() {
		return "<array type> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return at ArrayType object Creates new ArrayType object, parses the
	 *         types and sets the values for t1 and t2 variables. Returns the
	 *         ArrayType object
	 */
	static ArrayType parse(Scanner s) {

		enterParser("array type");

		ArrayType at = new ArrayType(s.curLineNum());

		// skip array [
		s.skip(arrayToken);
		s.skip(leftBracketToken);

		// parse Type
		at.t = Type.parse(s);
		s.skip(rightBracketToken);
		s.skip(ofToken);

		at.t2 = Type.parse(s);

		leaveParser("array type");
		return at;
	}

	@Override
	void check(Block curScope, Library lib) {
		t.check(curScope, lib);
		t.check(curScope, lib);
	}

	@Override
	void genCode(CodeFile f) {
		// TODO Auto-generated method stub
		
	}
}
