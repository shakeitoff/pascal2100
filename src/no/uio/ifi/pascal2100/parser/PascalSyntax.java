package no.uio.ifi.pascal2100.parser;
import no.uio.ifi.pascal2100.main.CodeFile;
import no.uio.ifi.pascal2100.main.Main;

public abstract class PascalSyntax {
	public int lineNum;
	public int uniqueId;
	
	PascalSyntax(int n) {
		lineNum = n;
	}

	boolean isInLibrary() {
		return lineNum < 0;
	}

	abstract void check(Block curScope, Library lib);
	
	abstract void genCode(CodeFile f);
	

	// Del 2:
	abstract public String identify();
	
	abstract void prettyPrint();

	void error(String message) {
		Main.error("Error at line " + lineNum + ": " + message);
	}

	static void enterParser(String nonTerm) {
		Main.log.enterParser(nonTerm);
	}

	static void leaveParser(String nonTerm) {
		Main.log.leaveParser(nonTerm);
	}
}
