package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
/**
 * Abstract Operator class, extends PascalSyntax 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public abstract class Operator extends PascalSyntax{

   Operator(int lineNum){
      super(lineNum);

   }

   public String identify() {
      return "<operator> on line " + lineNum;
   }


   void prettyPrint() {
      this.prettyPrint();
   }
   /**
    * @param s Scanner object
    * @return op Operator object
    * creates an empty Operator object,
    * finds what kind of operator it and parses it.
    * Op is set to refrence this object, then it is returned.
    */
   static Operator parse(Scanner s) {

      Operator op = null;

      if(s.curToken.kind.isFactorOpr()) {
	 op = FactorOperator.parse(s);
      } else if(s.curToken.kind.isPrefixOpr()) {
	 op = PrefixOperator.parse(s);
      } else if(s.curToken.kind.isRelOpr()) {
	 op = RelOperator.parse(s);
      } else if(s.curToken.kind.isTermOpr()) {
	 op = TermOperator.parse(s);
      }

      return op;
   }
}
