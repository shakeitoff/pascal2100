package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.List;

/**
 * StatmList class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

class StatmList extends Statement {
	List<Statement> statmList;

	StatmList(int lineNum) {
		super(lineNum);
		statmList = new ArrayList<Statement>();
	}

	@Override
	public void prettyPrint() {
		int i = 1;
		for (Statement st : statmList) {
			st.prettyPrint();

			if (i < statmList.size()) {
				Main.log.prettyPrint("; ");
				Main.log.prettyPrintLn();

			}
			i++;
		}
	}

	@Override
	public String identify() {
		return "<statm list> on line " + lineNum;

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return sl StatmList object Creates new StatmList object, parses and adds
	 *         the statements, returns the StatmList object.
	 */
	static StatmList parse(Scanner s) {
		enterParser("statm list");
		StatmList sl = new StatmList(s.curLineNum());

		sl.statmList.add(Statement.parse(s));

		while (s.curToken.kind == semicolonToken) {
			s.skip(semicolonToken);
			sl.statmList.add(Statement.parse(s));
		}

		leaveParser("statm list");
		return sl;
	}

	@Override
	void check(Block curScope, Library lib) {
		for (Statement st : statmList) {
			st.check(curScope, lib);
		}
	}

	@Override
	void genCode(CodeFile f) {
		for(Statement s: statmList) {
		    s.genCode(f);
		}
		
	}
}
