package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * InnerExpr class, extends Factor
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class InnerExpr extends Factor {

	Expression expr;

	InnerExpr(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<inner expr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("(");
		expr.prettyPrint();
		Main.log.prettyPrint(")");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return inExp InnerExpression object Creates new InnerExpr object, skips
	 *         "(" parses the expression, sets the local variable expr to
	 *         refrence it, skips ")" and returns the inner expression
	 */
	static InnerExpr parse(Scanner s) {
		enterParser("inner expr");
		InnerExpr inExp = new InnerExpr(s.curLineNum());

		s.skip(leftParToken);
		inExp.expr = Expression.parse(s);
		s.skip(rightParToken);

		leaveParser("inner expr");
		return inExp;
	}

	@Override
	void check(Block curScope, Library lib) {
		expr.check(curScope, lib);

	}

	@Override
	void genCode(CodeFile f) {
		expr.genCode(f);
	}
}
