package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * VarDeclpart class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class VarDeclPart extends PascalSyntax {
	List<VarDecl> vdList;

	VarDeclPart(int lineNum) {
		super(lineNum);
		vdList = new ArrayList<VarDecl>();
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("var ");
		for (VarDecl v : vdList) {
			v.prettyPrint();
		}
	}

	@Override
	public String identify() {
		return "<var decl part> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return vdp VarDeclPart object skips var token then adds variable
	 *         declarations to vdList list. Returns VarDeclPart object
	 */
	static VarDeclPart parse(Scanner s) {
		enterParser("var decl part");

		s.skip(varToken);
		VarDeclPart vdp = new VarDeclPart(s.curLineNum());
		vdp.vdList.add(VarDecl.parse(s));

		while (s.nextToken.kind == colonToken) {
			vdp.vdList.add(VarDecl.parse(s));
		}

		leaveParser("var decl part");
		return vdp;
	}

	@Override
	void check(Block curScope, Library lib) {
		for (VarDecl v : vdList) {
			v.check(curScope, lib);
		}
	}

	@Override
	void genCode(CodeFile f) {
		int offset = -36;

		for(VarDecl vd: vdList) {
			vd.declOffset = offset;
		    vd.genCode(f);
		    offset -= 4;
		}
		
	}

}
