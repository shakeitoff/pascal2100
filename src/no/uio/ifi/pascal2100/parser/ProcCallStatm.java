package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * ProcCallStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ProcCallStatm extends Statement {

    List<Expression> exprList;
    String name;
    ProcDecl procRef;

    ProcCallStatm(int lineNum) {
        super(lineNum);
        exprList = new ArrayList<Expression>();
    }

    @Override
    public void prettyPrint() {
        Main.log.prettyPrint(name);

        if (!exprList.isEmpty()) {
            Main.log.prettyPrint("(");

            Iterator<Expression> it = exprList.iterator();

            if (it.hasNext()) {
                it.next().prettyPrint();
            }

            while (it.hasNext()) {
                Main.log.prettyPrint(", ");
                it.next().prettyPrint();
            }

            Main.log.prettyPrint(")");
        }
    }

    @Override
    public String identify() {
        return "<proc call statm> on line " + lineNum;
    }

    /**
     * @param s
     *            Scanner object
     * @return pc ProcCallStatm object checks for name token, sets local name
     *         variable, parses the expressions and adds them to the exprList
     *         list. returns the ProcCallStatm object.
     */
    static ProcCallStatm parse(Scanner s) {
        enterParser("proc call statm");

        s.test(nameToken);
        ProcCallStatm pc = new ProcCallStatm(s.curLineNum());
        pc.name = s.curToken.id;
        s.readNextToken();

        if (s.curToken.kind == leftParToken) {
            s.skip(leftParToken);
            pc.exprList.add(Expression.parse(s));

            while (s.curToken.kind == commaToken) {
                s.skip(commaToken);
                pc.exprList.add(Expression.parse(s));
            }

            s.skip(rightParToken);
        }

        leaveParser("proc call statm");
        return pc;
    }

    @Override
    public void check(Block curScope, Library lib) {
        PascalDecl pd = curScope.findDecl(name, this);
        procRef = (ProcDecl) pd;

        for (Expression e : exprList) {
            e.check(curScope, lib);
        }

    }

    @Override
    void genCode(CodeFile f) {
        int size = 1;
        if (!name.equals("write")) {
            int revSize = exprList.size();
            Collections.reverse(exprList);
            for (Expression e : exprList) {
                e.genCode(f);
                f.genInstr("", "pushl", "%eax", "Push param #" + revSize);
                revSize--;
            }

            f.genInstr("", "call", "proc$" + this.name.toLowerCase() + "_" + procRef.uniqueId, "");
            f.genInstr("", "addl", "$" + (4 * exprList.size()) + ",%esp", "Remove parameters from stack");
        } else {
            for (Expression e : exprList) {
                Term t = (Term) e.simpleExp.terms.get(0);

                PascalSyntax c = t.list.get(0);
                
                if (e.simpleExp.preOpr != null || e.simpleExp.terms.size() > 1 || t.list.size() > 1) {
                    e.genCode(f);
                    f.genInstr("", "pushl", "%eax", "");
                }
                

                if (c instanceof CharLiteral) {
                    CharLiteral ch = (CharLiteral) c;
                    genChar(f, ch, size);
                } else if (c instanceof StringLiteral) {
                    StringLiteral st = (StringLiteral) c;
                    genString(f, st, size);
                } else if (c instanceof NumberLiteral) {
                    NumberLiteral nl = (NumberLiteral) c;
                    genNum(f, nl, size);
                } else if (c instanceof Variable) {

                    Variable val = (Variable) c;

                    if (val.name.equals("eol")) {
                        f.genInstr("", "movl", "$10,%eax", val.name);
                        f.genInstr("", "pushl", "%eax", "pushing param #" + size + " " + val.name);
                        f.genInstr("", "call", "write_char", "");
                        f.genInstr("", "addl", "$4,%esp", "");
                    } else {
                        if (val.varRef instanceof ConstDecl) {
                            ConstDecl cd = (ConstDecl) val.varRef;
                            if (cd.con instanceof NumberLiteral) {
                                NumberLiteral n = (NumberLiteral) cd.con;
                                genNum(f, n, size);
                            } else if (cd.con instanceof StringLiteral) {
                                StringLiteral s = (StringLiteral) cd.con;
                                genString(f, s, size);
                            } else if (cd.con instanceof CharLiteral) {
                                CharLiteral cha = (CharLiteral) cd.con;
                                genChar(f, cha, size);
                            }

                        } else if (val.varRef instanceof VarDecl || val.varRef instanceof EnumLiteral) {
                            val.genCode(f);
                            f.genInstr("", "pushl", "%eax", "pushing param #" + size + " " + val.name);
                            f.genInstr("", "call", "write_int", "");
                            f.genInstr("", "addl", "$4,%esp", "");
                        } else if (val.varRef instanceof ParamDecl) {
                            // :TODO Handle ParamDecl Instances
                            
                            if(e.simpleExp.preOpr == null && (e.simpleExp.terms.size() <= 1 && t.list.size() <= 1)) {
                                //ParamDecl par = (ParamDecl) val.varRef;
                                val.genCode(f);
                            }
                            f.genInstr("", "pushl", "%eax", " Push param #" + size + " " + val.name );
                            f.genInstr("", "call", "write_int", "");
                            f.genInstr("", "addl", "$4,%esp", "");

                        } else {
                            System.out.println("ERROR: procCallStatm.java 156: incompatible type -> " + c.getClass());
                        }
                    }
                } else if (c instanceof Negation) {
                    Negation n = (Negation) c;
                    n.genCode(f);
                    f.genInstr("", "pushl", "%eax", "");
                    f.genInstr("", "call", "write_int", "");
                    f.genInstr("", "addl", "$4,%esp", "");
                }

                size++;
            }
        }
    }

    public void genChar(CodeFile f, CharLiteral ch, int size) {
        ch.genCode(f);
        f.genInstr("", "pushl", "%eax", "pushing param #" + size);
        f.genInstr("", "call", "write_char", "");
        f.genInstr("", "addl", "$4,%esp", "Pop char");
    }

    public void genNum(CodeFile f, NumberLiteral nl, int size) {
        // nl.genCode(f);
        f.genInstr("", "movl", "$" + nl.value + ",%eax", "" + nl.value);
        f.genInstr("", "pushl", "%eax", "pushing param #" + size);
        f.genInstr("", "call", "write_int", "writing " + nl.value);
        f.genInstr("", "addl", "$4,%esp", "Pop int");
    }

    public void genString(CodeFile f, StringLiteral st, int size) {
        String label = f.getLocalLabel();
        f.genInstr("", ".data", "", "");
        f.genInstr(label, ".asciz", "\"" + st.strVal + "\"", "");

        f.genInstr("", ".align", "" + 2, "");
        f.genInstr("", ".text", "", "");
        f.genInstr("", "leal", label + ",%eax", "");
        f.genInstr("", "pushl", "%eax", "pushing param #" + size);
        f.genInstr("", "call", "write_string", "");
        f.genInstr("", "addl", "$4,%esp", "Pop parameter");

    }
}
