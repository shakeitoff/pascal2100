package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * StringLiteral class, extends Constant
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class StringLiteral extends Constant {
	String strVal;

	StringLiteral(int lineNum) {

		super(lineNum);
	}

	@Override
	public String identify() {
		return "<string literal> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("'");
		Main.log.prettyPrint(strVal);
		Main.log.prettyPrint("'");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return stLit StringLiteral object Tests for stringValToken, creates new
	 *         StringLiteral sets the value of local String strVal to the
	 *         current tokens strVal value. Reads the next token and returns the
	 *         StringLiteral object.
	 */
	static StringLiteral parse(Scanner s) {
		enterParser("string literal");
		s.test(stringValToken);

		StringLiteral stLit = new StringLiteral(s.curLineNum());
		stLit.strVal = s.curToken.strVal;
		s.readNextToken();
		leaveParser("string literal");
		return stLit;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {

	}
}
