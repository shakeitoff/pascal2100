package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * RelOperator class, extends Operator
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class RelOperator extends Operator {

	TokenKind tk;

	public RelOperator(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<rel opr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(" " + tk.toString() + " ");
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return ro RelOperator object creates a new RelOperator object checks
	 *         what kind of relational operator the current token is sets the
	 *         local TokenKind tk to this value returns the RelOperatori object
	 *         ro.
	 */
	static RelOperator parse(Scanner s) {
		enterParser("rel opr");
		RelOperator ro = new RelOperator(s.curLineNum());

		switch (s.curToken.kind) {
		case equalToken: // =
			ro.tk = equalToken;
			s.skip(equalToken);
			break;
		case greaterToken: // >
			ro.tk = greaterToken;
			s.skip(greaterToken);
			break;
		case notEqualToken: // <>
			ro.tk = notEqualToken;
			s.skip(notEqualToken);
			break;
		case lessEqualToken: // <=
			ro.tk = lessEqualToken;
			s.skip(lessEqualToken);
			break;
		case greaterEqualToken: // >=
			ro.tk = greaterEqualToken;
			s.skip(greaterEqualToken);
			break;
		case lessToken:
			ro.tk = lessToken;
			s.skip(lessToken);
			break;
		default:
			System.out.println("RelOperator unknown type of token: " + s.curToken.identify());
			break;
		}

		leaveParser("rel opr");
		return ro;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
	    f.genInstr("", "popl", "%ecx", "");
	    f.genInstr("", "cmpl", "%eax,%ecx", "");
	    f.genInstr("", "movl", "$0,%eax", "");
	    
		switch(tk) {
		case equalToken:
		    f.genInstr("", "sete", "%al", "=");
		    break;
		case greaterToken:
		    f.genInstr("", "setg", "%al", ">");
		    break;
		case notEqualToken:
		    f.genInstr("", "setne", "%al", "<>");
		    break;
		case lessEqualToken:
		    f.genInstr("", "setle", "%al", "<=");
		    break;
		case greaterEqualToken:
		    f.genInstr("", "setge", "%al", ">=");
		    break;
		case lessToken:
		    f.genInstr("", "setl", "%al", "<");
		    break;
		}
		
	}
}
