package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * FuncCall class, extends Factor
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class FuncCall extends Factor {
	String name;
	List<Expression> expressions;
	FuncDecl funcRef;

	FuncCall(int lineNum) {
		super(lineNum);
		expressions = new ArrayList<Expression>();
	}

	@Override
	public String identify() {
		return "<func call> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(name);

		if (!expressions.isEmpty()) {
			Main.log.prettyPrint("(");

			Iterator<Expression> it = expressions.iterator();

			if (it.hasNext()) {
				it.next().prettyPrint();
			}

			while (it.hasNext()) {
				Main.log.prettyPrint(", ");
				it.next().prettyPrint();
			}
			Main.log.prettyPrint(")");
		}

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return fc FuncCall object Tests name token, creates new FuncCall object
	 *         Sets value of local String name to value of current tokens id.
	 *         Looks for "(" if found, parses the expression(s) and adds it/them
	 *         to the expressions arraylist. Skips ")" and returns the FuncCall
	 *         object.
	 */
	static FuncCall parse(Scanner s) {
		enterParser("func call");

		s.test(nameToken);
		FuncCall fc = new FuncCall(s.curLineNum());
		fc.name = s.curToken.id;
		s.readNextToken();

		if (s.curToken.kind == leftParToken) {
			s.skip(leftParToken);
			fc.expressions.add(Expression.parse(s));

			while (s.curToken.kind == commaToken) {
				s.skip(commaToken);
				fc.expressions.add(Expression.parse(s));
			}
			s.skip(rightParToken);
		}

		leaveParser("func call");
		return fc;
	}

	@Override
	void check(Block curScope, Library lib) {
		PascalDecl pd = curScope.findDecl(name, this);
		funcRef = (FuncDecl)pd;
		
		for(Expression e: expressions) {
			e.check(curScope, lib);
		}

	}

	@Override
	void genCode(CodeFile f) {
		Collections.reverse(expressions);
		int size = expressions.size();
		for(Expression e: expressions) {
			e.genCode(f);
			f.genInstr("", "pushl", "%eax", "Push param #" + size);
			size--;
		}
		
		f.genInstr("", "call", "func$"+ this.name + "_" + funcRef.uniqueId, "");
		f.genInstr("", "addl", "$" + (4*expressions.size()) +",%esp", "Remove parameters from stack");
		
	}
}
