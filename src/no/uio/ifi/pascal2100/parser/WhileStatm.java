package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.CodeFile.*;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.Scanner;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * WhileStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

class WhileStatm extends Statement {
	Expression expr;
	Statement body;

	WhileStatm(int lineNum) {
		super(lineNum);
	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint("while ");
		expr.prettyPrint();
		Main.log.prettyPrint(" do ");
		Main.log.prettyIndent();
		body.prettyPrint();
		Main.log.prettyOutdent();
	}

	@Override
	public String identify() {

		return "<while statm> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return ws WhileStatm object skips while, parses the expression and sets
	 *         local value, skips the do, parses and sets local body object.
	 *         return WhileStatm object
	 */
	static WhileStatm parse(Scanner s) {
		enterParser("while statm");

		WhileStatm ws = new WhileStatm(s.curLineNum());
		s.skip(whileToken);

		ws.expr = Expression.parse(s);
		s.skip(doToken);
		ws.body = Statement.parse(s);

		leaveParser("while statm");
		return ws;
	}

	@Override
	void check(Block curScope, Library lib) {
		expr.check(curScope, lib);
		body.check(curScope, lib);
	}

	@Override
	public void genCode(CodeFile f) {

		String testLabel = f.getLocalLabel(), endLabel = f.getLocalLabel();
		f.genInstr(testLabel, "", "", "Start while-statement");
		expr.genCode(f);
		f.genInstr("", "cmpl", "$0,%eax", "");
		f.genInstr("", "je", endLabel, "");
		body.genCode(f);
		f.genInstr("", "jmp", testLabel, "");
		f.genInstr(endLabel, "", "", "End while-statement");

	}
}
