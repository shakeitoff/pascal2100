package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * AssignStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class AssignStatm extends Statement {

    Variable var;
    Expression expr;

    AssignStatm(int lineNum) {
        super(lineNum);
    }

    @Override
    public void prettyPrint() {
        var.prettyPrint();
        Main.log.prettyPrint(" := ");
        expr.prettyPrint();
    }

    @Override
    public String identify() {
        return "<assign statm> on line " + lineNum;
    }

    /**
     * @param s
     *            Scanner object
     * @return as AssignStatm object Creates AssignStatm object, sets local
     *         variable and expression values returns AssignStatm object.
     */
    static AssignStatm parse(Scanner s) {
        enterParser("assign statm");
        AssignStatm as = new AssignStatm(s.curLineNum());

        as.var = Variable.parse(s);
        s.skip(assignToken);
        as.expr = Expression.parse(s);

        leaveParser("assign statm");
        return as;
    }

    @Override
    void check(Block curScope, Library lib) {
        var.check(curScope, lib);
        expr.check(curScope, lib);
    }

    @Override
    void genCode(CodeFile f) {
        expr.genCode(f);

        if (var.varRef instanceof FuncDecl) {
            f.genInstr("", "movl", "%eax,-32(%ebp)", "");
        } else if (var.varRef instanceof VarDecl) {
        	f.genInstr("", "movl", "" + (-4 * var.varRef.declLevel) + "(%ebp),%edx", "");
            f.genInstr("", "movl", "%eax," + var.varRef.declOffset + "(%edx)", var.name + " :=");
        }
    }
}
