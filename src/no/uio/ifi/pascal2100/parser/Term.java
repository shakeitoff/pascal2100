package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Term class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class Term extends PascalSyntax {

    List<PascalSyntax> list;

    public Term(int n) {
        super(n);
    }

    @Override
    public String identify() {
        return "<term> on line " + lineNum;
    }

    @Override
    void prettyPrint() {
        for (PascalSyntax ps : list) {

            ps.prettyPrint();
        }
    }

    /**
     * @param s
     *            Scanner object
     * @return term Term object Creates new term object, parses the factor and
     *         adds it list list, parses and adds factor operators and factors
     *         to the list list. Returns the Term object
     */
    static Term parse(Scanner s) {
        enterParser("term");

        Term term = new Term(s.curLineNum());
        term.list = new ArrayList<PascalSyntax>();
        term.list.add(Factor.parse(s));

        while (s.curToken.kind.isFactorOpr()) {
            term.list.add(FactorOperator.parse(s));
            term.list.add(Factor.parse(s));
        }
        leaveParser("term");
        return term;
    }

    @Override
    void check(Block curScope, Library lib) {
        for (PascalSyntax s : list) {
            s.check(curScope, lib);
        }

    }

    @Override
    void genCode(CodeFile f) {

        List<FactorOperator> oprList = new ArrayList<FactorOperator>();
        List<Factor> factList = new ArrayList<Factor>();
        int factCount = 0;
        int oprCount = 0;

        for (PascalSyntax ps : list) {
            if (ps instanceof Factor) {
                factList.add((Factor) ps);
                factCount++;
            } else if (ps instanceof FactorOperator) {
                oprList.add((FactorOperator) ps);
                oprCount++;
            }
        }

        if (factCount >= 2 && oprCount >= 1) {
            if (factCount == 2 && oprCount == 1) {
                int i = 0;
                for (PascalSyntax ps : list) {
                    if (ps instanceof Factor && i < 1) {
                        Factor fac = (Factor) ps;
                        fac.genCode(f);
                        f.genInstr("", "pushl", "%eax", "");
                        i++;
                    } else if (ps instanceof Factor) {
                        Factor fac = (Factor) ps;
                        fac.genCode(f);
                    }
                }

                for (PascalSyntax ps : list) {
                    if (ps instanceof FactorOperator) {
                        FactorOperator facOpr = (FactorOperator) ps;
                        facOpr.genCode(f);
                    }
                }
            } else if (factCount > 2 && oprCount > 1) {
                Iterator<Factor> factIt = factList.iterator();
                Iterator<FactorOperator> oprIt = oprList.iterator();

                Factor f1 = factIt.next();
                f1.genCode(f);
                f.genInstr("", "pushl", "%eax", "");
                Factor f2 = factIt.next();
                f2.genCode(f);
                FactorOperator fo1 = oprIt.next();
                fo1.genCode(f);

                while (oprIt.hasNext()) {
                    f.genInstr("", "pushl", "%eax", "");
                    Factor f3 = factIt.next();
                    f3.genCode(f);
                    FactorOperator fo2 = oprIt.next();
                    fo2.genCode(f);
                }
            }
        } else if (factCount == 1 && oprCount == 0) {
            for (PascalSyntax ps : list) {
                if (ps instanceof Factor) {
                    Factor fact = (Factor) ps;
                    fact.genCode(f);
                }
            }
        }

    }
}
