package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Program class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class Program extends PascalDecl {
   Block block;

   Program(String id, int lineNum) {
      super(id, lineNum);

   }

   @Override
      public void prettyPrint() {
	 Main.log.prettyPrint("program ");
	 Main.log.prettyPrint(this.name);
	 Main.log.prettyPrint(";");
	 Main.log.prettyPrintLn();
	 block.prettyPrint();
	 Main.log.prettyPrint(".");

      }

   @Override
      public String identify() {
	 return "<program> on line " + lineNum;
      }

   /**
    * @param s
    *            Scanner object
    * @return p Program object skips program token, creates new Program object,
    *         parses block and sets local block variable, returns Program
    *         object.
    */
   public static Program parse(Scanner s) {
      enterParser("program");

      s.skip(programToken);

      s.test(nameToken);
      Program p = new Program(s.curToken.id, s.curLineNum());
      s.readNextToken();

      s.skip(semicolonToken);
      p.block = Block.parse(s);
      s.skip(dotToken);

      leaveParser("program");
      return p;
   }

   @Override
      public void check(Block curScope, Library lib) {
	 block.check(curScope, lib);

      }

   @Override
      public void genCode(CodeFile f) {
	 String label = f.getLocalLabel();
	 //Generate instruction for external library
	 f.genInstr("", ".extern", "write_char", "");
	 f.genInstr("", ".extern", "write_int", "");
	 f.genInstr("", ".extern", "write_string", "");
	 f.genInstr("", ".globl", "_main", "");
	 f.genInstr("", ".globl", "main", "");

	 //Create "fake" main function
	 f.genInstr("_main", "", "", ""); //Windows/MacOS main function
	 f.genInstr("main", "call", "prog$" + this.name.toLowerCase() + "_" + 1, "Start program"); //Call the real main function from the "fake" one
	 f.genInstr("", "movl", "$0,%eax", "Set status 0 and"); //Give out status message and send it to eax
	 f.genInstr("", "ret", "", "terminate the program"); //return

	 block.genCodeProcFunc(f);
	 f.genInstr("prog$" + this.name.toLowerCase() +  "_" + 1, "", "", ""); //Start of the "real" main function that is called above
	 f.genInstr("", "enter", "$" + (32 + block.getLocalVars()) + ",$1", "");
	 block.genCode(f);
	 f.genInstr("", "leave", "", "");
	 f.genInstr("", "ret", "", ""); //leave and return up to "fake" main when program is finished generating assembler

      }

}
