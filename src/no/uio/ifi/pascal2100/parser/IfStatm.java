package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * IfStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class IfStatm extends Statement {

	Expression expr;
	Statement body;
	Statement altBody;

	IfStatm(int lineNum) {
		super(lineNum);
	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint("if ");
		expr.prettyPrint();
		Main.log.prettyPrint(" then ");
		Main.log.prettyPrintLn();
		Main.log.prettyIndent();
		body.prettyPrint();
		Main.log.prettyOutdent();

		if (altBody != null) {
			Main.log.prettyPrintLn();
			Main.log.prettyPrint(" else ");
			Main.log.prettyPrintLn();
			Main.log.prettyIndent();
			altBody.prettyPrint();
			Main.log.prettyOutdent();

		}
	}

	@Override
	public String identify() {
		return "<if statm> on line " + lineNum;

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return ifStat IfStatm object Creates new IfStatm object skips the if
	 *         parses expression and sets local expr value. skips the then,
	 *         parses and sets local body value. checks for alternative body, if
	 *         its there, parse and set the value, return IfStatm object.
	 */
	static IfStatm parse(Scanner s) {
		enterParser("if statm");

		IfStatm ifStat = new IfStatm(s.curLineNum());

		s.skip(ifToken);
		ifStat.expr = Expression.parse(s);

		s.skip(thenToken);
		ifStat.body = Statement.parse(s);

		if (s.curToken.kind == elseToken) {
			s.skip(elseToken);
			ifStat.altBody = Statement.parse(s);
		}

		leaveParser("if statm");
		return ifStat;
	}

	@Override
	void check(Block curScope, Library lib) {
		expr.check(curScope, lib);
		body.check(curScope, lib);
		
		if(altBody != null) {
			altBody.check(curScope, lib);
		}

	}

	@Override
	void genCode(CodeFile f) {
	    String endLabel = f.getLocalLabel();
	    String testLabel = f.getLocalLabel();
		if(altBody == null) {
		    f.genInstr("", "", "", "Start if-statement");
			expr.genCode(f);
			f.genInstr("", "cmpl", "$0,%eax", "");
			f.genInstr("", "je", testLabel, "");
			body.genCode(f);
			f.genInstr(testLabel, "", "", "End if-statement");
		} else {
			f.genInstr("", "", "", "Start if-statement");
			expr.genCode(f);
			f.genInstr("", "cmpl", "$0,%eax", "");
			f.genInstr("", "je", endLabel, "");
			body.genCode(f);
			f.genInstr("", "jmp", testLabel, "");
			f.genInstr(endLabel, "", "", "Else-statement");
			altBody.genCode(f);
			f.genInstr(testLabel, "", "", "End if-statement");
		}
	}
}
