package no.uio.ifi.pascal2100.parser;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import no.uio.ifi.pascal2100.main.CodeFile.*;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Block part of PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class Block extends PascalSyntax {

	ConstDeclPart constDecPart;
	TypeDeclPart typeDecPart;
	VarDeclPart varDecPart;
	List<PascalDecl> declList;
	StatmList statList;
	HashMap<String, PascalDecl> decls;
	Block outerScope;
	static int blockLevel = 1;
	static int uId = 2;

	Block(int lineNum) {
		super(lineNum);
		decls = new HashMap<String, PascalDecl>();
	}

	@Override
	public String identify() {
		return "<block> on line " + lineNum;

	}

	/**
	 * Used to add declaration to the hashmap of declarations
	 * 
	 * @param id
	 *            the name of the binding
	 * @param d
	 *            the declaration the name is bound to
	 */
	void addDecl(String id, PascalDecl d) {
		if (decls.containsKey(id)) {
			d.error(id + " declared twice in the same block.");
		}

		decls.put(id, d);
	}

	/**
	 * Checks the contents of block and adds all declarations to the hashmap
	 * that contains all declarations
	 * 
	 * @param curScope
	 *            the current scope
	 * @param lib
	 *            library
	 */
	@Override
	public void check(Block curScope, Library lib) {
		outerScope = curScope;

		if (constDecPart != null) {
			constDecPart.check(this, lib);
			for (ConstDecl cd : constDecPart.cdList) {
				addDecl(cd.name.toLowerCase(), cd);
			}
		}

		if (typeDecPart != null) {
			typeDecPart.check(this, lib);
		}

		if (varDecPart != null) {
			varDecPart.check(this, lib);
			for (VarDecl vd : varDecPart.vdList) {
				addDecl(vd.name.toLowerCase(), vd);
			}
		}

		if (declList != null) {
			for (PascalDecl pd : declList) {
				if (pd != null) {
					if (pd instanceof FuncDecl) {
						FuncDecl f = (FuncDecl) pd;
						f.uniqueId = uId;
						addDecl(f.name.toLowerCase(), f);
						f.check(this, lib);
					} else if (pd instanceof ProcDecl) {
						ProcDecl p = (ProcDecl) pd;
						p.uniqueId = uId;
						addDecl(p.name.toLowerCase(), p);
						p.check(this, lib);
					}
					uId++;
				}
			}
		}

		if (statList != null) {
			statList.check(this, lib);
		}

	}

	/**
	 * Used to see if declaration is declared in scope, if it is then it logs
	 * the binding and returns the declaration. If not found it check the outer
	 * scope and does the same thing. If still not found, gives an error and
	 * returns null;
	 * 
	 * @param id
	 * @param where
	 * @return PascalDecl returns the declaration
	 */
	PascalDecl findDecl(String id, PascalSyntax where) {

		PascalDecl d = decls.get(id.toLowerCase());

		if (d != null) {
			Main.log.noteBinding(id, where, d);
			return d;
		}
		if (outerScope != null) {
			return outerScope.findDecl(id, where);
		}

		where.error("Name " + id + " is unknown.");
		return null;
	}

	/**
	 * prettyprints the different parts of block.
	 */
	@Override
	void prettyPrint() {
		if (constDecPart != null) {
			constDecPart.prettyPrint();
			Main.log.prettyPrintLn();
		}
		if (typeDecPart != null) {
			typeDecPart.prettyPrint();
			Main.log.prettyPrintLn();
		}
		if (varDecPart != null) {
			varDecPart.prettyPrint();
			Main.log.prettyPrintLn();
		}

		for (PascalDecl ps : declList) {
			if (ps instanceof FuncDecl || ps instanceof ProcDecl) {
				ps.prettyPrint();
			} else {
				System.out.println("Unknown type in declList: " + ps.identify());
			}
		}
		Main.log.prettyPrint("begin");
		Main.log.prettyPrintLn();
		Main.log.prettyIndent();

		statList.prettyPrint();

		Main.log.prettyOutdent();
		Main.log.prettyPrintLn();
		Main.log.prettyPrint("end");

	}

	/**
	 * Creates Block object, calls parse method on all the parts of Block.
	 * 
	 * @param s
	 *            Scanner object.
	 * @return block object.
	 */
	static Block parse(Scanner s) {
		enterParser("block");
		Block block = new Block(s.curLineNum());
		block.declList = new ArrayList<PascalDecl>();

		if (s.curToken.kind == constToken) {
			block.constDecPart = ConstDeclPart.parse(s);
		}

		if (s.curToken.kind == typeToken) {
			block.typeDecPart = TypeDeclPart.parse(s);
		}

		if (s.curToken.kind == varToken) {
			block.varDecPart = VarDeclPart.parse(s);
		}

		while (s.curToken.kind == functionToken || s.curToken.kind == procedureToken) {
			if (s.curToken.kind == functionToken) {
				block.declList.add(FuncDecl.parse(s));
			} else if (s.curToken.kind == procedureToken) {
				block.declList.add(ProcDecl.parse(s));
			}
		}

		s.skip(beginToken);
		block.statList = StatmList.parse(s);
		s.skip(endToken);
		leaveParser("block");
		return block;
	}

	public void genCodeProcFunc(CodeFile f) {
		blockLevel++;
		if (declList != null) {
			for (PascalDecl pd : declList) {
				if (pd != null) {
					if (pd instanceof FuncDecl) {
						FuncDecl func = (FuncDecl) pd;
						func.declLevel = blockLevel;
						func.genCode(f);
					} else if (pd instanceof ProcDecl) {
						ProcDecl proc = (ProcDecl) pd;
						proc.declLevel = blockLevel;
						proc.genCode(f);
					}
				}
			}
		}
		blockLevel--;
	}

	public int getLocalVars() {
		int localVars = 0;
		if (varDecPart != null) {
			localVars = varDecPart.vdList.size() * 4;
		}
		return localVars;
	}

	@Override
	public void genCode(CodeFile f) {

		if (constDecPart != null) {
			for (ConstDecl c : constDecPart.cdList) {
				c.declLevel = blockLevel;
			}
			//constDecPart.genCode(f);
		}

		if (typeDecPart != null) {
			for (TypeDecl t : typeDecPart.tdList) {
				t.declLevel = blockLevel;
			}
			typeDecPart.genCode(f);
		}

		if (varDecPart != null) {
			for (VarDecl v : varDecPart.vdList) {
				v.declLevel = blockLevel;
			}
			varDecPart.genCode(f);
		}

		if (statList != null) {
			statList.genCode(f);
		}

	}

}
