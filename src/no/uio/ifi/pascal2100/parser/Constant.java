package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Abstract constant class, extends Factor
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public abstract class Constant extends Factor {

	Constant(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<constant> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return Constant object Creates an empty constant-object "constant" finds
	 *         out what kind of constant the current token is, parses it and
	 *         sets "constant" to refrence the object returned from the parse.
	 *         Then returns "constant".
	 */
	static Constant parse(Scanner s) {
		enterParser("constant");

		Constant constant = null;

		switch (s.curToken.kind) {
		case intValToken:
			constant = NumberLiteral.parse(s);
			break;
		case stringValToken:
			if (s.curToken.strVal.length() > 1) {
				constant = StringLiteral.parse(s);
			} else {
				constant = CharLiteral.parse(s);
			}
			break;
		default:
			constant = NamedConst.parse(s);
			break;
		}

		leaveParser("constant");
		return constant;
	}
}
