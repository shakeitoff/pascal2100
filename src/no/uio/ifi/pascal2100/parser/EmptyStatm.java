package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;

/**
 * EmptyStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

class EmptyStatm extends Statement {

	EmptyStatm(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<empty statm> on line " + lineNum;
	}

	@Override
	public void prettyPrint() {

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return es EmptyStatm object Creates empty statement object and returns
	 *         it.
	 */
	static EmptyStatm parse(Scanner s) {
		enterParser("empty statm");
		EmptyStatm es = new EmptyStatm(s.curLineNum());

		leaveParser("empty statm");
		return es;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
		//Doesnt need any code
	}
}
