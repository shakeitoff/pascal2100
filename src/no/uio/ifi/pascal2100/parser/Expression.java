package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.CodeFile.*;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * Expression extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class Expression extends PascalSyntax {

    SimpleExpr simpleExp;
    RelOperator relOpr;
    SimpleExpr optionalExp;

    Expression(int lineNum) {

        super(lineNum);
    }

    @Override
    public String identify() {
        return "<expression> on line " + lineNum;
    }

    /**
     * prettyprints the simple expression, and then the optional rel opr/simple
     * expression if there is any.
     */
    @Override
    void prettyPrint() {
        simpleExp.prettyPrint();

        if (relOpr != null && optionalExp != null) {
            relOpr.prettyPrint();
            optionalExp.prettyPrint();
        }
    }

    /**comment
     * First parses the simple expression, then parses the optional rel
     * opr/simple expression if there is any.
     * 
     * @param s
     *            Scanner object
     */
    static Expression parse(Scanner s) {
        enterParser("expression");

        Expression exp = new Expression(s.curLineNum());
        exp.simpleExp = SimpleExpr.parse(s);

        if (s.curToken.kind.isRelOpr()) { // checks for optional rel opr
            exp.relOpr = RelOperator.parse(s);
            exp.optionalExp = SimpleExpr.parse(s);
        }

        leaveParser("expression");
        return exp; // returns the Expression object
    }

    @Override
    void check(Block curScope, Library lib) {
        simpleExp.check(curScope, lib);

        if (relOpr != null && optionalExp != null) {
            relOpr.check(curScope, lib);
            optionalExp.check(curScope, lib);
        }

    }

    @Override
    public void genCode(CodeFile f) {
        simpleExp.genCode(f);
               
        if(relOpr != null ) {
        	f.genInstr("", "pushl", "%eax", "EXPRESSION 1");
            optionalExp.genCode(f);
            relOpr.genCode(f);
        }
    }
}
