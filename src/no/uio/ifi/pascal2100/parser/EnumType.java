package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * EnumType class, extends Type
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class EnumType extends Type {
   List<EnumLiteral> el;
   int value = 0;
   EnumType(int lineNum) {
      super(lineNum);
      el = new ArrayList<EnumLiteral>();
   }

   @Override
      public void prettyPrint() {
	 Main.log.prettyPrint("(");

	 Iterator<EnumLiteral> it = el.iterator();
	 if (it.hasNext()) {
	    it.next().prettyPrint();
	 }

	 while (it.hasNext()) {
	    Main.log.prettyPrint(", ");
	    it.next().prettyPrint();
	 }
      }

   @Override
      public String identify() {
	 return "<enum type> on line " + lineNum;
      }

   /**
    * @param s
    *            Scanner object
    * @return et EnumType object Creates new EnumType object, parses and adds
    *         enum literals to the el list returns the EnumType object.
    */
   static EnumType parse(Scanner s) {
      enterParser("enum type");

      EnumType et = new EnumType(s.curLineNum());

      s.skip(leftParToken);
       
      EnumLiteral en = EnumLiteral.parse(s);

      en.value = et.value;
      et.value++;

      et.el.add(en);

      while (s.curToken.kind == commaToken) {
	 s.skip(commaToken);
	 en = EnumLiteral.parse(s);
	 en.value = et.value;
	 et.value++;
	 et.el.add(en);
	 
      }
      s.skip(rightParToken);

      leaveParser("enum type");
      return et;
   }

   @Override
      void check(Block curScope, Library lib) {
	 for (EnumLiteral en : el) {
	    en.check(curScope, lib);
	 }
      }

   @Override
      void genCode(CodeFile f) {

      }
}
