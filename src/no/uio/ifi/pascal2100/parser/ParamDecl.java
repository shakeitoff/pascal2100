package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * ParamDecl class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ParamDecl extends PascalDecl {

	Type type;

	public ParamDecl(String id, int lNum) {
		super(id, lNum);
	}

	@Override
	public String identify() {
		return "<param decl> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(name);
		Main.log.prettyPrint(": ");
		type.prettyPrint();

	}

	/**
	 * @param s
	 *            Scanner object
	 * @return pd ParamDecl object tests for name token, creates news ParamDecl
	 *         object parses the type and sets local type refrence. returns the
	 *         ParamDecl.
	 */
	static ParamDecl parse(Scanner s) {
		enterParser("param decl");

		s.test(nameToken);

		ParamDecl pd = new ParamDecl(s.curToken.id, s.curLineNum());
		s.readNextToken();

		s.skip(colonToken);
		pd.type = Type.parse(s);

		leaveParser("param decl");
		return pd;

	}

	@Override
	void check(Block curScope, Library lib) {
		type.check(curScope, lib);

	}

	@Override
	void genCode(CodeFile f) {
		type.genCode(f);
	}

}
