package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * VarDecl class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class VarDecl extends PascalDecl {

	Type type;

	VarDecl(String id, int lineNum) {
		super(id, lineNum);
	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint(name + ": ");
		type.prettyPrint();
		Main.log.prettyPrint("; ");
	}

	@Override
	public String identify() {
		return "<var decl> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return vd VarDecl object checks for name token, creates new VarDecl
	 *         object, skips colon token sets local Type object returns VarDecl
	 *         object
	 */
	static VarDecl parse(Scanner s) {
		enterParser("var decl");

		s.test(nameToken);
		VarDecl vd = new VarDecl(s.curToken.id, s.curLineNum());
		s.readNextToken();

		s.skip(colonToken);

		vd.type = Type.parse(s);
		s.skip(semicolonToken);
		leaveParser("var decl");
		return vd;
	}

	@Override
	void check(Block curScope, Library lib) {
		type.check(curScope, lib);
	}

	@Override
	void genCode(CodeFile f) {
		type.genCode(f);
		
	}

}
