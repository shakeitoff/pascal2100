package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * ProcDecl class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class ProcDecl extends PascalDecl {

	ParamDeclList pdl;
	Block block;

	public ProcDecl(String id, int lNum) {
		super(id, lNum);
	}

	@Override
	public String identify() {
		if (lineNum > 0) {
			return "<proc decl> on line " + lineNum;
		} else {
			return "<proc decl>";
		}
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("procedure ");
		Main.log.prettyPrint(name);

		if (pdl != null) {
			pdl.prettyPrint();
		} else {
			Main.log.prettyIndent();
		}

		Main.log.prettyPrint(";");
		Main.log.prettyPrintLn();
		block.prettyPrint();
		Main.log.prettyPrint(";");
		if (pdl != null) {
			Main.log.prettyOutdent();
		}
		Main.log.prettyPrintLn();
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return procD ProcDelc object Skips procedure, tests for name token,
	 *         creates a new ProcDecl object checks for parameter declaration
	 *         list , if there is one sets local ParamDeclList object to
	 *         refrence the parse of this list. Otherwise parse the block and
	 *         return the ProcDecl object.
	 */
	static ProcDecl parse(Scanner s) {
		enterParser("proc decl");

		s.skip(procedureToken);

		s.test(nameToken);
		ProcDecl procD = new ProcDecl(s.curToken.id, s.curLineNum());
		s.readNextToken();

		if (s.curToken.kind == semicolonToken) {
			s.skip(semicolonToken);
			procD.block = Block.parse(s);
			s.skip(semicolonToken);
			leaveParser("proc-decl");
			return procD;
		} else {
			procD.pdl = ParamDeclList.parse(s);
			s.skip(semicolonToken);
			procD.block = Block.parse(s);
			s.skip(semicolonToken);
			leaveParser("proc decl");
			return procD;
		}
	}

	@Override
	void check(Block curScope, Library lib) {

		if (pdl != null) {
			for (ParamDecl pd : pdl.pdList) {
				block.addDecl(pd.name.toLowerCase(), pd);
			}
			pdl.check(curScope, lib);
		}

		block.check(curScope, lib);

	}

	@Override
	void genCode(CodeFile f) {

		String label = f.getLocalLabel();
		block.genCodeProcFunc(f);

		if (pdl != null) {
			for (ParamDecl pd : pdl.pdList) {
				pd.declLevel = this.declLevel;
			}
			pdl.genCode(f);
		}

		f.genInstr("proc$" + this.name.toLowerCase() + "_" + uniqueId, "", "", "");
		f.genInstr("", "enter", "$" + (32 + block.getLocalVars()) + ",$" + this.declLevel, "Start of " + this.name);

		block.genCode(f);

		f.genInstr("", "leave", "", "#Leave proc decl " + this.name);
		f.genInstr("", "ret", "", "");
	}
}
