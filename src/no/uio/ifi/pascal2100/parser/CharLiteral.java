package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.Scanner;

/**
 * CharLiteral class, extends Constant
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class CharLiteral extends Constant {

	String name;

	CharLiteral(int lineNum) {

		super(lineNum);
	}

	@Override
	public String identify() {
		return "<char literal> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint("'");
		Main.log.prettyPrint(name);
		Main.log.prettyPrint("'");
	}

	/**
	 * @param s
	 *            Scanner object.
	 * @return chLit CharLiteral object. creats a new CharLiteral, sets its
	 *         local name variable. Reads the next token and returns the newly
	 *         created CharLiteral.
	 */
	static CharLiteral parse(Scanner s) {
		enterParser("char literal");

		CharLiteral chLit = new CharLiteral(s.curLineNum());
		chLit.name = s.curToken.strVal;
		s.readNextToken();

		leaveParser("char literal");
		return chLit;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
		
		char ch = name.charAt(0);
		int chVal = (int)ch;
		f.genInstr("", "movl", "$"+chVal+",%eax", ""+name);
	}
}

