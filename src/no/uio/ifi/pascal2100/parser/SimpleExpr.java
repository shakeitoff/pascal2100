package no.uio.ifi.pascal2100.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import no.uio.ifi.pascal2100.main.CodeFile;
import no.uio.ifi.pascal2100.scanner.Scanner;

/**
 * SimpleExpr class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class SimpleExpr extends PascalSyntax {

	PrefixOperator preOpr;
	List<PascalSyntax> terms;

	public SimpleExpr(int n) {
		super(n);

	}

	@Override
	public String identify() {
		return "<simple expr> on line " + lineNum;
	}

	@Override
	void prettyPrint() {

		if (preOpr != null) {
			preOpr.prettyPrint();
		}

		for (PascalSyntax tm : terms) {
			tm.prettyPrint();
		}
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return sExpr SimpleExpr object Creates simple expression object, adds
	 *         prefix operator if there is one otherwise parse term/term
	 *         operators and add them to local terms list. return SimpleExpr
	 *         object.
	 */

	static SimpleExpr parse(Scanner s) {
		enterParser("simple expr");
		SimpleExpr sExpr = new SimpleExpr(s.curLineNum());

		sExpr.terms = new ArrayList<PascalSyntax>();

		if (s.curToken.kind.isPrefixOpr()) {
			sExpr.preOpr = PrefixOperator.parse(s);
		}

		sExpr.terms.add(Term.parse(s));

		while (s.curToken.kind.isTermOpr()) {
			sExpr.terms.add(TermOperator.parse(s));
			sExpr.terms.add(Term.parse(s));
		}

		leaveParser("simple expr");
		return sExpr;
	}

	@Override
	void check(Block curScope, Library lib) {
		if (preOpr != null) {
			preOpr.check(curScope, lib);
		}

		for (PascalSyntax s : terms) {
			s.check(curScope, lib);
		}
	}
	

	@Override
	void genCode(CodeFile f) {

		List<TermOperator> oprList = new ArrayList<TermOperator>();
		List<Term> termList = new ArrayList<Term>();
		int termCount = 0;
		int oprCount = 0;
		
		for (PascalSyntax ps : terms) {
			if (ps instanceof Term) {
				termList.add((Term) ps);
				termCount++;
			} else if (ps instanceof TermOperator) {
				oprList.add((TermOperator) ps);
				oprCount++;
			}
		}
		
		if (termCount >= 2 && oprCount >= 1) {
			if(termCount == 2 && oprCount == 1) {
				int i = 0;
				for (PascalSyntax ps : terms) {
					if (ps instanceof Term && i < 1) {
						Term t = (Term) ps;
						t.genCode(f);
						f.genInstr("", "pushl", "%eax", "");
						i++;
					} else if (ps instanceof Term) {
						Term t = (Term) ps;
						t.genCode(f);
					}
				}
				for (PascalSyntax ps : terms) {
					if (ps instanceof TermOperator) {
						TermOperator to = (TermOperator) ps;
						to.genCode(f);
					}
				}
			} else if(termCount > 2 && oprCount > 1) {
				Iterator<Term> termIt = termList.iterator();
				Iterator<TermOperator> oprIt = oprList.iterator();
				
				Term t1 = termIt.next();
				t1.genCode(f);
				f.genInstr("", "pushl", "%eax", "");
				Term t2 = termIt.next();
				t2.genCode(f);
				TermOperator to1 = oprIt.next();
				to1.genCode(f);
				
				while(oprIt.hasNext()) {
					f.genInstr("", "pushl", "%eax", "");
					Term t3 = termIt.next();
					t3.genCode(f);
					TermOperator to2 = oprIt.next();
					to2.genCode(f);
				}			
			}
		} else if(termCount == 1 && oprCount == 0){
			for (PascalSyntax ps : terms) {
				if (ps instanceof Term) {
					Term t = (Term) ps;
					t.genCode(f);
				}
			}
		}
	      if (preOpr != null) {
	            preOpr.genCode(f);
	        }
	}
}
