package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * RangeType class, extends Type
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class RangeType extends Type {

	Constant consLeft;
	Constant consRight;

	public RangeType(int lineNum) {
		super(lineNum);
	}

	@Override
	public void prettyPrint() {
		consLeft.prettyPrint();
		Main.log.prettyPrint(" .. ");
		System.out.println("RangeType: consRight.identify: " + consRight.identify());
		consRight.prettyPrint();
	}

	@Override
	public String identify() {
		return "<range type> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return rt RangeType object creates new RangeType object, sets local left
	 *         and right constant values, returns the RangeType object.
	 */
	static RangeType parse(Scanner s) {
		enterParser("range type");

		RangeType rt = new RangeType(s.curLineNum());
		rt.consLeft = Constant.parse(s);
		s.skip(rangeToken);
		rt.consRight = Constant.parse(s);

		leaveParser("range type");
		return rt;

	}

	@Override
	void check(Block curScope, Library lib) {
		consLeft.check(curScope, lib);
		consRight.check(curScope, lib);
	}

	@Override
	void genCode(CodeFile f) {
		consLeft.genCode(f);
		consRight.genCode(f);
	}

}
