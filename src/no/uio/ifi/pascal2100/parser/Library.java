package no.uio.ifi.pascal2100.parser;

import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.main.CodeFile.*;

import java.util.HashMap;

/**
 * Library extends block, used as top of recursion for the program acts like a
 * fake block with declarations for boolean/true/false write/char/eol.
 *
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class Library extends Block {
	// TODO: cant send "this" to AddDecl
	TypeDecl ch;
	TypeDecl bool;
	ConstDecl eol;
	ProcDecl write;
	TypeDecl integ;

	public Library(int lineNr) {
		super(lineNr);

		this.decls = new HashMap<String, PascalDecl>();

		// boolean
		this.bool = new TypeDecl("boolean", -1);
		this.bool.t = new EnumType(0);
		this.bool.tn = new TypeName(0);
		this.bool.tn.name = "boolean";

		EnumType et = (EnumType) this.bool.t;

		EnumLiteral tru = new EnumLiteral("true", -1);
		EnumLiteral fal = new EnumLiteral("false", -1);
		et.el.add(tru);
		et.el.add(fal);

		this.addDecl("boolean", bool);
		this.addDecl("true", tru);
		this.addDecl("false", fal);

		// write
		this.write = new ProcDecl("write", -1);
		this.addDecl("write", write);

		// eol
		this.eol = new ConstDecl("eol", -1);
		this.addDecl("eol", eol);

		// char
		this.ch = new TypeDecl("char", -1);
		this.ch.tn = new TypeName(0, "char");
		this.ch.t = this.ch.tn;
		this.addDecl("char", ch);

		// Integer
		this.integ = new TypeDecl("integer", -1);
		this.integ.tn = new TypeName(0, "integer");
		this.integ.t = this.integ.tn;
		this.addDecl("integer", integ);

	}

	@Override
	public String identify() {
		return "in the Library";
	}

	@Override
	void prettyPrint() {

	}
	@Override
	public void genCode(CodeFile f){
	 }

}
