package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * abstract Statement class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

abstract class Statement extends PascalSyntax {

	Statement(int lineNum) {
		super(lineNum);
	}

	@Override
	public String identify() {
		return "<statement> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return st Statement object Creates empty statement object, checks for
	 *         what kind of statement the current token is, sets the statement
	 *         object to what is returned from the parse, then return that
	 *         statement.
	 */
	static Statement parse(Scanner s) {
		enterParser("statement");

		Statement st = null;

		switch (s.curToken.kind) {
		case beginToken:
			st = CompoundStatm.parse(s);
			break;
		case ifToken:
			st = IfStatm.parse(s);
			break;
		case nameToken:
			switch (s.nextToken.kind) {
			case assignToken:
			case leftBracketToken:
				st = AssignStatm.parse(s);
				break;
			default:
				st = ProcCallStatm.parse(s);
				break;
			}
			break;
		case whileToken:
			st = WhileStatm.parse(s);
			break;
		default:
			st = EmptyStatm.parse(s);
			break;
		}

		leaveParser("statement");
		return st;
	}
}
