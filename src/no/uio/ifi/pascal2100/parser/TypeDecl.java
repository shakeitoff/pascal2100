package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * TypeDecl class, extends PascalDecl
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class TypeDecl extends PascalDecl {

   TypeName tn;
   Type t;

   public TypeDecl(String id, int lNum) {
      super(id, lNum);

   }

   @Override
      public String identify() {
	 if(lineNum > 0) {
	    return "<type decl> on line " + lineNum;
	 } else {
	    return "<type decl>";
	 }
      }

   @Override
      void prettyPrint() {
	 tn.prettyPrint();
	 Main.log.prettyPrint(" = ");
	 t.prettyPrint();
	 Main.log.prettyPrint(";");
      }

   /**
    * @param s
    *            Scanner object
    * @return td TypeDecl object creates new TypeDecl object, sets local
    *         TypeName and Type values, return TypeDecl object.
    */
   static TypeDecl parse(Scanner s) {
      enterParser("type decl");

      TypeDecl td = new TypeDecl(s.curToken.id, s.curLineNum());
      td.tn = TypeName.parse(s);

      s.skip(equalToken);
      td.t = Type.parse(s);
      s.skip(semicolonToken);

      leaveParser("type decl");
      return td;
   }

   @Override
      void check(Block curScope, Library lib) {
	 curScope.addDecl(tn.name.toLowerCase(), this);
	 tn.check(curScope, lib);
	 t.check(curScope, lib);
      }

   @Override
      void genCode(CodeFile f) {
	 tn.genCode(f);
	 t.genCode(f);

      }

}
