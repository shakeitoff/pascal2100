package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * CompoundStatm class, extends Statement
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

class CompoundStatm extends Statement {

	StatmList sl;

	CompoundStatm(int lineNum) {
		super(lineNum);
	}

	@Override
	public void prettyPrint() {
		Main.log.prettyPrint("begin");
		Main.log.prettyPrintLn();
		Main.log.prettyIndent();
		sl.prettyPrint();
		Main.log.prettyPrintLn();
		Main.log.prettyOutdent();
		Main.log.prettyPrint("end");
	}

	@Override
	public String identify() {
		return "<compound statm> on line " + lineNum;
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return sc CompoundStatm Creates new compound statement object, sets
	 *         local StatmList object. Return CompundStatm object.
	 */
	static CompoundStatm parse(Scanner s) {
		enterParser("compund statm");

		CompoundStatm cs = new CompoundStatm(s.curLineNum());
		s.skip(beginToken);
		cs.sl = StatmList.parse(s);
		s.skip(endToken);

		leaveParser("compound statm");
		return cs;
	}

	@Override
	void check(Block curScope, Library lib) {
		sl.check(curScope, lib);

	}

	@Override
	void genCode(CodeFile f) {
		sl.genCode(f);
		
	}
}
