package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

import no.uio.ifi.pascal2100.scanner.Scanner;

/**
 * Variable class, extends Factor
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class Variable extends Factor {

    String name;
    Expression expr;
    PascalDecl varRef;

    public Variable(int lineNum) {
        super(lineNum);

    }

    @Override
    public void prettyPrint() {
        Main.log.prettyPrint(name);

        if (expr != null) {
            Main.log.prettyPrint("[");
            expr.prettyPrint();
            Main.log.prettyPrint("]");
        }
    }

    @Override
    public String identify() {
        return "<variable> on line " + lineNum;
    }

    /**
     * @param s
     *            Scanner object
     * @return v Variable object Tests for name token, creates a new Variable
     *         object sets the value of local String name to the current tokens
     *         id. looks for left bracket, if there is one parses it and sets
     *         the local Expression expr to reference this object.i skips the
     *         right bracket token and returns the Variable object.
     */
    static Variable parse(Scanner s) {
        enterParser("variable");

        s.test(nameToken);
        Variable v = new Variable(s.curLineNum());
        v.name = s.curToken.id;

        s.readNextToken();

        if (s.curToken.kind == leftBracketToken) {

            s.skip(leftBracketToken);
            v.expr = Expression.parse(s);
            s.skip(rightBracketToken);
        }

        leaveParser("variable");
        return v;
    }

    void check(Block curScope, Library lib) {

        PascalDecl vd = curScope.findDecl(name, this);
        varRef = vd;

        if (expr != null) {
            expr.check(curScope, lib);
        }
    }

    @Override
    void genCode(CodeFile f) {
        if (expr != null) {
            expr.genCode(f);
        }

        if (varRef instanceof EnumLiteral) {
            EnumLiteral el = (EnumLiteral) varRef;
            el.genCode(f);
        } else if (varRef instanceof ParamDecl) {
            f.genInstr("", "movl", "" + (-4 * varRef.declLevel) + "(%ebp),%edx", "");
            f.genInstr("", "movl", "" + varRef.declOffset + "(%edx),%eax", name);

        } else if (varRef instanceof ConstDecl) {
            ConstDecl cd = (ConstDecl) varRef;
            cd.genCode(f);

        } else if (varRef instanceof VarDecl) {
            f.genInstr("", "movl", "" + (-4 * varRef.declLevel) + "(%ebp),%edx", "");
            f.genInstr("", "movl", "" + varRef.declOffset + "(%edx),%eax", name);
        } else {

            System.out.println("ERROR: Unhandled variable type: ");
        }
    }
}
