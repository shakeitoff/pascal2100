package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * NumberLiteral class, extends Constant
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class NumberLiteral extends Constant {
	int value;

	NumberLiteral(int lineNum) {

		super(lineNum);
	}

	@Override
	public String identify() {
		return "<number literal> on line " + lineNum;
	}

	@Override
	void prettyPrint() {
		Main.log.prettyPrint(Integer.toString(value));
	}

	/**
	 * @param s
	 *            Scanner object
	 * @return nl NumberLiteral object Creates new NumberLiteral object, sets
	 *         the local value integer to be the current tokens intVal value
	 *         skips the intValToken and returns the NumberLiteral object.
	 */
	static NumberLiteral parse(Scanner s) {
		enterParser("number literal");

		NumberLiteral nl = new NumberLiteral(s.curLineNum());
		nl.value = s.curToken.intVal;
		s.skip(intValToken);
		leaveParser("number literal");
		return nl;
	}

	@Override
	void check(Block curScope, Library lib) {

	}

	@Override
	void genCode(CodeFile f) {
		f.genInstr("", "movl", "$"+value+",%eax", ""+value);
	}
}
