package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * TypeDeclPart class, extends PascalSyntax
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */
public class TypeDeclPart extends PascalSyntax {

   List<TypeDecl> tdList;

   public TypeDeclPart(int n) {
      super(n);
      tdList = new ArrayList<TypeDecl>();
   }

   @Override
      public String identify() {
	 return "<type decl part> on line " + lineNum;
      }

   @Override
      void prettyPrint() {
	 Main.log.prettyPrint("type ");

	 Iterator<TypeDecl> it = tdList.iterator();

	 if (it.hasNext()) {
	    it.next().prettyPrint();
	 }

	 while (it.hasNext()) {
	    it.next().prettyPrint();
	 }
      }

   /**
    * @param s
    *            Scanner object
    * @return tdp TypeDeclPart object creates new TypeDeclPart object, skips
    *         type token, adds type declarations to tdList list, returns
    *         TypeDeclPart object.
    */
   static TypeDeclPart parse(Scanner s) {
      enterParser("type decl part");
      TypeDeclPart tdp = new TypeDeclPart(s.curLineNum());

      s.skip(typeToken);

      tdp.tdList.add(TypeDecl.parse(s));

      while (s.curToken.kind == nameToken) {
	 tdp.tdList.add(TypeDecl.parse(s));
      }

      leaveParser("type decl part");
      return tdp;
   }

   @Override
      void check(Block curScope, Library lib) {
	 for (TypeDecl t : tdList) {
	    if (t != null) {
	       t.check(curScope, lib);
	    }
	 }

      }

   @Override
      void genCode(CodeFile f) {
	 for(TypeDecl td: tdList) {
	    td.genCode(f);
	 }

      }

}
