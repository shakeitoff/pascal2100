package no.uio.ifi.pascal2100.parser;

import no.uio.ifi.pascal2100.main.*;
import no.uio.ifi.pascal2100.scanner.*;
import static no.uio.ifi.pascal2100.scanner.TokenKind.*;

/**
 * TypeName class, extends Type
 * 
 * @author mortefjo
 * @author cjgrauco
 * @version 19/10/2015
 */

public class TypeName extends Type {

   String name;
   TypeDecl typeRef;

   public TypeName(int lineNum) {
      super(lineNum);
   }

   public TypeName(int lineNum, String name) {
      super(lineNum);
      this.name = name;
   }

   @Override
      public void prettyPrint() {
	 Main.log.prettyPrint(name);
      }

   @Override
      public String identify() {
	 return "<type name> on line " + lineNum;
      }

   /**
    * @param s
    *            Scanner object
    * @return tn TypeName object Tests for name token, creates new TypeName
    *         object, sets local name variable to be current tokens id value.
    *         returns TypeName object
    */
   static TypeName parse(Scanner s) {
      enterParser("type name");
      s.test(nameToken);
      TypeName tn = new TypeName(s.curLineNum());
      tn.name = s.curToken.id;
      
      s.readNextToken();

      leaveParser("type name");
      return tn;

   }

   @Override
      void check(Block curScope, Library lib) {
	 PascalDecl pd = curScope.findDecl(name, this);
	 typeRef = (TypeDecl) pd;
      }

   @Override
      void genCode(CodeFile f) {

      }

}
