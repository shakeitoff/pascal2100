package no.uio.ifi.pascal2100.scanner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import no.uio.ifi.pascal2100.main.Main;

public class Scanner {
	public Token curToken = null, nextToken = null;

	private LineNumberReader sourceFile = null;
	private String sourceFileName, sourceLine = "", fullLine = ""; // fullLine
																	// used to
																	// keep
																	// entire
																	// line for
																	// errors
	private int sourcePos = 0;
	private int lineNr = 0;
	private String fullString = ""; // used for textStrings inside ' '

	private static final Pattern intConst = Pattern.compile("[0-9]+");
	private static final Pattern stringConst = Pattern.compile("[a-zA-Z]+[a-zA-Z0-9_]*");

	public Scanner(String fileName) {
		sourceFileName = fileName;
		try {
			sourceFile = new LineNumberReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			Main.error("Cannot read " + fileName + "!");
		}

		skipEmptyLines();
		readNextToken();
		readNextToken();

	}

	public String identify() {
		return "Scanner reading " + sourceFileName;
	}

	public int curLineNum() {
		return curToken.lineNum;
	}

	private void error(String message) {
		Main.error("Scanner error on line " + lineNr + ": " + message);
	}

	/**
	 * Skips empty lines
	 */
	private void skipEmptyLines() {
		while ((sourceLine.isEmpty() || sourceLine.trim().equals("") || sourceLine.trim().equals("\n")) && sourceFile != null) {
			readNextLine();
			fullLine = sourceLine;
		}
	}

	/**
	 * Reads in the next line, if the line is not empty adds to the lineNr
	 * variable. Checks if the next one in line is a Token or matches either
	 * integer or string regex. If all these checks return false, print error
	 * message. It then logs the token. If the curToken is a end-token and the
	 * nextToken is a dot-token, then set next to be a eof-token so we terminate
	 * the while-loop since we have then reached the end of the file. Also logs
	 * the eof-token.
	 */
	public void readNextToken() {
		
		if (curToken != null && nextToken != null) {
			if (curToken.kind == TokenKind.endToken && nextToken.kind == TokenKind.dotToken) {
				Token t = new Token(TokenKind.eofToken, lineNr);
				curToken = nextToken;
				nextToken = t;
				Main.log.noteToken(nextToken);
				return;
			} else if (curToken.kind == TokenKind.dotToken && nextToken.kind == TokenKind.eofToken) {
				curToken = nextToken;
				return;
			}
		}
				
		skipEmptyLines();
		isComment();
		
		curToken = nextToken;

		/*
		 * sourceFile is set to null if there is no more lines to read in the
		 * file which means we can see if we have reached end without finding
		 * eof-token
		 */
		if (sourceFile == null) {
			Main.error("Reached end of file without finding eof-token.");
			System.exit(-1);
		}

		if (sourceLine.isEmpty()) {
			readNextLine();
			fullLine = sourceLine;
		}

		skipWhitespace();

		if (!sourceLine.isEmpty()) {
			boolean ok = isString() || isComment() || tryToken() || tryRegex(intConst, TokenKind.intValToken)
					|| tryRegex(stringConst, TokenKind.nameToken);

			if (!ok) {
				error(fullLine + "\n" + "Failed to create token of identifier at position " + sourcePos + ", Token: "
						+ sourceLine.charAt(0));
			}

			skipWhitespace();
		}
	}

	/**
	 * Checks the sourceLine for either a { or /* to see if its a comment. If it
	 * is, it tries finding the closing comment, if it is not found on the same
	 * line it reads in a new line and continues until it finds it.
	 * 
	 * @return true Returns true if it is a comment and closing has been found
	 */
	private boolean isComment() {
		int startingCommentLine = lineNr + 1;

		if (sourceLine.startsWith("{")) {
			while (!findClosingCurlyBracket()) {
				if (sourceLine.equals("")) {
					Main.error(
							"Scanner error: comment starting at line " + startingCommentLine + " is never terminated.");
					System.exit(-1);
				} else {
					readNextLine();
					fullLine = sourceLine;
				}
			}
			readNextLine();
			fullLine = sourceLine;
			return true;
		} else if (sourceLine.startsWith("/*")) {
			while (!findClosingSlashStar()) {
				if (sourceLine.equals("")) {
					Main.error(
							"Scanner error: comment starting at line " + startingCommentLine + " is never terminated.");
					System.exit(-1);
				} else {
					readNextLine();
					fullLine = sourceLine;
				}

			}
			readNextLine();
			fullLine = sourceLine;
			return true;
		}
		return false;
	}

	/**
	 * Method looks for the closing /* in the sourceLine, if it is not there it
	 * returns false. If it is there it consumes the input and returns true.
	 * 
	 * @return True If closing comment is found.
	 */
	private boolean findClosingSlashStar() {
		int i = 0;
		while (i < sourceLine.length()) {
			if (sourceLine.charAt(i) == '*' && sourceLine.charAt(i + 1) == '/') {
				consumeInput(i + 1);
				return true;
			}
			i++;
		}
		return false;
	}

	/**
	 * Method looks for the closing curly bracket in the sourceLine, if it is
	 * found it returns true, else it returns false.
	 * 
	 * @return True If closing curly bracket is found
	 */
	private boolean findClosingCurlyBracket() {
		int i = 0;
		while (i < sourceLine.length()) {
			if (sourceLine.charAt(i) == '}') {
				consumeInput(i + 1);
				return true;
			}
			i++;
		}
		return false;
	}

	/**
	 * Checks if it is a string enclosed by ' If it it build up the string until
	 * we find a closing '. If not found log/print error message and exit
	 * compiler. If found create a stringValToken/text string token and add to
	 * list
	 * 
	 * @return true returns true if string is found and closed properly
	 */
	private boolean isString() {
		int startingCommentLine = lineNr + 1;

		if (sourceLine.startsWith("'")) {
			while (!findClosingApos()) {
				if (sourceLine.equals("")) {
					Main.error(
							"Scanner error: string starting at line " + startingCommentLine + " is never terminated.");
					System.exit(-1);
				} else {
					readNextLine();
					fullLine = sourceLine;
				}
			}
			Token t = new Token("", fullString, lineNr);
			curToken = nextToken;
			nextToken = t;
			Main.log.noteToken(nextToken);
			fullLine = sourceLine;
			fullString = "";
			return true;
		}
		return false;
	}

	/**
	 * Searches for the closing apostrophe in the sourceLine. if found consumes
	 * the input and return true. Else it builds up the text string.
	 * 
	 * @return true returns true if closing apostrophe is found
	 */
	private boolean findClosingApos() {
		int i = 1;
		while (i < sourceLine.length()) {
			if (sourceLine.charAt(i) == '\'' && sourceLine.charAt(i + 1) != '\'') {
				consumeInput(i + 1);
				return true;
			} else if (sourceLine.charAt(i) == '\'' && sourceLine.charAt(i + 1) == '\'') {
				fullString += "'";
				i++;
			} else {
				fullString += sourceLine.charAt(i);
			}
			i++;
		}
		return false;
	}

	/**
	 * Tries to match the pattern designed for a numeric value or a string
	 * literal. It then checks if it's a digit and calls on Integer.parseInt to
	 * get the value and then creates a intValToken out of it. Else it creates a
	 * nameToken.
	 * 
	 * @param pattern
	 *            The regex pattern to match with
	 * @param tk
	 *            the tokenKind of the input
	 * @return true if it matches regex pattern, else false
	 */
	private boolean tryRegex(Pattern pattern, TokenKind tk) {
		Matcher m = pattern.matcher(sourceLine);
		if (m.lookingAt()) {
			Token t = new Token(tk, lineNr);
			if (isDigit(sourceLine.charAt(0))) {
				t.intVal = Integer.parseInt(m.group());
			} else {
				t.id = m.group();
			}
			t.lineNum = lineNr;
			consumeInput(m.end());
			curToken = nextToken;
			nextToken = t;
			Main.log.noteToken(nextToken);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Iterates through all the TokeKinds to see if it matches what the line
	 * start with. It handles cases where we are using tokens that consist of
	 * two characters (e.g '..', '>=', '<=', '<>'). It then calls createToken to
	 * make the tokens and add them to the tokenList. If it does not match any
	 * of the special cases mentioned above it checks if it matches with any of
	 * the other tokenKinds (e.g. 'program', 'do', 'const'...), if it does then
	 * create the token.
	 * 
	 * @return true if token is found, else false
	 */
	private boolean tryToken() {
		if (sourceLine.startsWith(".") && sourceLine.charAt(1) == '.') { // Handles
																			// the
																			// ".."
																			// token
			createToken(TokenKind.rangeToken);
			return true;
		} else if (sourceLine.startsWith(">") && sourceLine.charAt(1) == '=') { // Handles
																				// ">="
																				// token
			createToken(TokenKind.greaterEqualToken);
			return true;
		} else if (sourceLine.startsWith("<") && sourceLine.charAt(1) == '=') { // Handles
																				// "<="
																				// token
			createToken(TokenKind.lessEqualToken);
			return true;
		} else if (sourceLine.startsWith("<") && sourceLine.charAt(1) == '>') { // Handles
																				// the
																				// "<>"
																				// token
			createToken(TokenKind.notEqualToken);
			return true;
		} else {

			for (TokenKind tk : TokenKind.values()) {
				if (sourceLine.startsWith(tk.toString())) { // else match with a
															// token from
					createToken(tk); // the TokenKind enum and create token is
										// called
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Creates a new token and adds it to the tokenList. Then consumes the input
	 * and sets the curToken and nextToken variables.
	 * 
	 * @param tk
	 *            The tokenKind of the token you want to create
	 */
	private void createToken(TokenKind tk) {
		Token newToken = new Token(tk, lineNr);
		newToken.lineNum = lineNr;
		consumeInput(tk.toString().length());
		curToken = nextToken;
		nextToken = newToken;
		Main.log.noteToken(nextToken);
	}

	/**
	 * Checks for whitespace in the sourceLine variable, skips it and then
	 * consumes the input
	 */
	private void skipWhitespace() {
		int i = 0;
		while (i < sourceLine.length() && Character.isWhitespace(sourceLine.charAt(i))) {
			i++;
		}
		consumeInput(i);
	}

	/**
	 * Consumes the input if it either matches a TokenKind or a whitespace and
	 * alters the sourcePos variable that keeps track of far into the line we
	 * are. If it is a newline it also adds to the lineNr variable.
	 * 
	 * It then sets the sourceLine variable to be equal to the substring of the
	 * amount, it returns the rest of the string without the part we have just
	 * worked on.
	 * 
	 * @param amount
	 *            the amount of characters we want to consume
	 */
	private void consumeInput(int amount) {
		for (int i = 0; i < amount; ++i) {
			char c = sourceLine.charAt(i);
			if (c == '\n') {
				lineNr++;
				sourcePos = 1;
			} else if (c == '\r') { // works with windows, handles carriage
									// return /r
			} else {
				sourcePos++;
			}
		}
		sourceLine = sourceLine.substring(amount);
	}

	private void readNextLine() {
		boolean alreadyLogged = false;
		if (sourceFile != null) {
			try {
				sourceLine = sourceFile.readLine();
				if (sourceLine == null) {
					sourceFile.close();
					sourceFile = null;
					sourceLine = "";
				} else if (sourceLine.isEmpty()) {
					readNextLine();
					alreadyLogged = true;
				} else {
					sourceLine += " ";
					alreadyLogged = false;
				}
				sourcePos = 0;
				lineNr++;
			} catch (IOException e) {
				Main.error("Scanner error: unspecified I/O error!");
			}
		}
		if (sourceFile != null && !alreadyLogged)
			Main.log.noteSourceLine(getFileLineNum(), sourceLine);
	}

	private int getFileLineNum() {
		return (sourceFile != null ? sourceFile.getLineNumber() : 0);
	}

	// Character test utilities:

	@SuppressWarnings("unused")
	private boolean isLetterAZ(char c) {
		return 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z';
	}

	private boolean isDigit(char c) {
		return '0' <= c && c <= '9';
	}

	// Parser tests:

	public void test(TokenKind t) {
		if (curToken.kind != t)
			testError(t.toString());
	}

	public void testError(String message) {
		Main.error(curLineNum(), "Expected a " + message + " but found a " + curToken.kind + "!");
	}

	public void skip(TokenKind t) {
		test(t);
		readNextToken();
	}
}
